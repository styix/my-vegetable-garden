using System.Globalization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.JSInterop;
using MudBlazor;
using MudBlazor.Services;
using MyVegetableGarden.App;
using MyVegetableGarden.App.i18n;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");
builder.Services.AddMudServices();
builder.Services.AddScoped(sp =>
    new HttpClient
    {
        BaseAddress = new Uri(builder.Configuration["ApiUrl"] ?? "http://localhost")
    });
builder.Services.AddLocalization();
builder.Services.AddTransient<MudLocalizer, ResXMudLocalizer>();

//var jsInterop = builder.Build().Services.GetRequiredService<IJSRuntime>();
// var appLanguage = await jsInterop.InvokeAsync<string>("appCulture.get");
// if (appLanguage != null)
// {
//     CultureInfo cultureInfo = new(appLanguage);
//     CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
//     CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
// }

await builder.Build().RunAsync();