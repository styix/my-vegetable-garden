using Microsoft.Extensions.Localization;
using MudBlazor;
using MyVegetableGarden.Models.I18n;

namespace MyVegetableGarden.App.i18n;

internal class ResXMudLocalizer : MudLocalizer
{
    private IStringLocalizer _localization;

    public ResXMudLocalizer(IStringLocalizer<Words> localizer)
    {
        _localization = localizer;
    }

    public override LocalizedString this[string key] => _localization[key];
}