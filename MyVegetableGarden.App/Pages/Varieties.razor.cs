using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.App.Pages.Dialogs;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.App.Pages;

public partial class Varieties
{
    private Paginated<List<Variety>> _PaginatedVarieties = null;
    [Parameter] public Plant _SelectedVarietyItem { get; set; }
    private string _SearchText;
    private MudDataGrid<Variety> _DataGrid;
    [CascadingParameter] MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }
    
    [Inject]
    public IDialogService DialogService { get; set; }

    private async Task<GridData<Variety>> _LoadGridData(GridState<Variety> state)
    {
        _PaginatedVarieties = await Http.GetFromJsonAsync<Paginated<List<Variety>>>($"/Variety?pageIndex={state.Page}&pageSize={state.PageSize}&search={_SearchText}");

        return new()
        {
            Items = _PaginatedVarieties.Items,
            TotalItems = _PaginatedVarieties.TotalCount
        };
    }
    
    private void _OnFilterValueChanged(string searchText)
    {
        if (string.IsNullOrWhiteSpace(searchText))
            return;
        
        _SearchText = searchText;
        _DataGrid.ReloadServerData();
    }

    private async Task _AddVarietyAsync()
    {
        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditVarietyDialog>(I18n["Adding"], dialogOptions);
        var result = await dialog.Result;
        
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }

    private async Task _EditVarietyAsync(Variety variety)
    {
        var parameters = new DialogParameters<EditVarietyDialog>();
        parameters.Add(e => e.CurrentVariety, variety);

        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditVarietyDialog>(I18n["Edition"], parameters, dialogOptions);
        var result = await dialog.Result;

        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }

    private async Task _RemoveVarietyAsync(Variety variety)
    {
        var parameters = new DialogParameters<RemoveVarietyDialog>();
        parameters.Add(e => e.CurrentVariety, variety);

        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<RemoveVarietyDialog>(I18n["Removal"], parameters, dialogOptions);
        var result = await dialog.Result;

        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
}