using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.App.Pages.Dialogs;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.App.Pages;

public partial class Providers
{
    private Paginated<List<Provider>> _PaginatedProviders = null;

    [Parameter]
    public Provider _SelectedProviderItem { get; set; }

    private string _SearchText;
    private MudDataGrid<Provider> _DataGrid;

    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public IDialogService DialogService { get; set; }

    private async Task<GridData<Provider>> _LoadGridData(GridState<Provider> state)
    {
        _PaginatedProviders =
            await Http.GetFromJsonAsync<Paginated<List<Provider>>>(
                $"/Provider?pageIndex={state.Page}&pageSize={state.PageSize}&search={_SearchText}");

        return new()
        {
            Items = _PaginatedProviders.Items,
            TotalItems = _PaginatedProviders.TotalCount
        };
    }

    private void _OnFilterValueChanged(string searchText)
    {
        if (string.IsNullOrWhiteSpace(searchText))
            return;

        _SearchText = searchText;
        _DataGrid.ReloadServerData();
    }

    private async Task _AddProviderAsync()
    {
        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditProviderDialog>(I18n["Adding"], dialogOptions);
        var result = await dialog.Result;

        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }

    private async Task _EditProviderAsync(Provider provider)
    {
        var parameters = new DialogParameters<EditProviderDialog>();
        parameters.Add(e => e.CurrentProvider, provider);

        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditProviderDialog>(I18n["Edition"], parameters, dialogOptions);
        var result = await dialog.Result;

        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }

    private async Task _RemoveProviderAsync(Provider provider)
    {
        var parameters = new DialogParameters<RemoveProviderDialog>();
        parameters.Add(e => e.CurrentProvider, provider);

        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<RemoveProviderDialog>(I18n["Removal"], parameters, dialogOptions);
        var result = await dialog.Result;

        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
}