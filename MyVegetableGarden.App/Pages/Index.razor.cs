using System.Net.Http.Json;
using BlazorCalendar.Models;
using Microsoft.AspNetCore.Components;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;
using Radzen;
using Radzen.Blazor;
using Radzen.Blazor.Rendering;

namespace MyVegetableGarden.App.Pages;

public partial class Index
{
    [Inject]
    public HttpClient Http { get; set; }

    private List<Seedling> _Seedlings;
    
    protected async override Task OnInitializedAsync()
    {
        Paginated<List<Seedling>> seedlings = await Http.GetFromJsonAsync<Paginated<List<Seedling>>>(
            $"/Seedling?pageIndex=0&pageSize=1000");
    
        List<Tasks> tasks = new List<Tasks>();
        
        if (seedlings != null && seedlings.Items != null)
            _Seedlings = seedlings.Items;
    }
    
    RadzenScheduler<Seedling> scheduler;
    Dictionary<DateTime, string> events = new Dictionary<DateTime, string>();
    Month startMonth = Month.January;

    // IList<Appointment> appointments = new List<Appointment>
    // {
    //     new Appointment { Start = DateTime.Today.AddDays(-2), End = DateTime.Today.AddDays(-2), Text = "Birthday" },
    //     new Appointment { Start = DateTime.Today.AddDays(-11), End = DateTime.Today.AddDays(-10), Text = "Day off" },
    //     new Appointment { Start = DateTime.Today.AddDays(-10), End = DateTime.Today.AddDays(-8), Text = "Work from home" },
    //     new Appointment { Start = DateTime.Today.AddHours(10), End = DateTime.Today.AddHours(12), Text = "Online meeting" },
    //     new Appointment { Start = DateTime.Today.AddHours(10), End = DateTime.Today.AddHours(13), Text = "Skype call" },
    //     new Appointment { Start = DateTime.Today.AddHours(14), End = DateTime.Today.AddHours(14).AddMinutes(30), Text = "Dentist appointment" },
    //     new Appointment { Start = DateTime.Today.AddDays(1), End = DateTime.Today.AddDays(12), Text = "Vacation" },
    // };

    async Task StartMonthChange()
    {
        await scheduler.Reload();
    }

    void OnSlotRender(SchedulerSlotRenderEventArgs args)
    {
        // Highlight today in month view
        if (args.View.Text == "Month" && args.Start.Date == DateTime.Today)
        {
            args.Attributes["style"] = "background: rgba(255,220,40,.2);";
        }

        // Draw a line for new year if start month is not January
        if ((args.View.Text == "Planner" || args.View.Text == "Timeline") && args.Start.Month == 12 && startMonth != Month.January)
        {
            args.Attributes["style"] = "border-bottom: thick double var(--rz-base-600);";
        }
    }

    async Task OnSlotSelect(SchedulerSlotSelectEventArgs args)
    {
        //console.Log($"SlotSelect: Start={args.Start} End={args.End}");

        // if(args.View.Text != "Year")
        // {            
        //     Appointment data = await DialogService.OpenAsync<AddAppointmentPage>("Add Appointment",
        //         new Dictionary<string, object> { { "Start", args.Start }, { "End", args.End } });
        //
        //     if (data != null)
        //     {
        //         appointments.Add(data);
        //         // Either call the Reload method or reassign the Data property of the Scheduler
        //         await scheduler.Reload();
        //     }
        // }
    }

    async Task OnAppointmentSelect(SchedulerAppointmentSelectEventArgs<Seedling> args)
    {
        //console.Log($"AppointmentSelect: Appointment={args.Data.Variety.Name}");

        //await DialogService.OpenAsync<EditAppointmentPage>("Edit Appointment", new Dictionary<string, object> { { "Appointment", args.Data } });

        await scheduler.Reload();
    }

    void OnAppointmentRender(SchedulerAppointmentRenderEventArgs<Seedling> args)
    {
        // Never call StateHasChanged in AppointmentRender - would lead to infinite loop

        // if (args.Data.Text == "Birthday")
        // {
        //     args.Attributes["style"] = "background: red";
        // }
    }
}