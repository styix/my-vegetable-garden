using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.App.Pages.Dialogs;

public partial class RemoveProviderDialog
{
    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ISnackbar Snackbar { get; set; }

    [Parameter]
    public Provider CurrentProvider { get; set; }

    private void _Close()
    {
        MudDialog.Close();
    }

    private async Task _RemoveAsync()
    {
        try
        {
            await Http.DeleteAsync($"/Provider/{CurrentProvider.Id}");
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        Snackbar.Add(I18n["ProviderSuccessfullyRemoved"], Severity.Success);

        MudDialog.Close(DialogResult.Ok(true));
    }
}