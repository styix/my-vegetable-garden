using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using MudBlazor;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.App.Pages.Dialogs;

public partial class EditPlantDialog
{
    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ISnackbar Snackbar { get; set; }

    [Parameter]
    public Plant CurrentPlant { get; set; } = new ();

    private void _Close()
    {
        MudDialog.Close();
    }

    private async Task _SubmitAsync()
    {
        if (CurrentPlant.Id > 0)
            await _UpdateAsync();
        else
            await _AddAsync();
    }
    
    private async Task _UpdateAsync()
    {
        int updatedRowCount = 0;

        try
        {
            var response = await Http.PutAsJsonAsync<Plant>($"/Plant/{CurrentPlant.Id}", CurrentPlant);
            updatedRowCount = await response.Content.ReadFromJsonAsync<int>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (updatedRowCount == 0)
            Snackbar.Add(I18n["NothingUpdated"], Severity.Info);
        else if (updatedRowCount == 1)
            Snackbar.Add(I18n["PlantSuccessfullyUpdated"], Severity.Success);

        MudDialog.Close(DialogResult.Ok<int>(updatedRowCount));
    }

    private async Task _AddAsync()
    {
        Plant createdPlant = null;

        try
        {
            var response = await Http.PostAsJsonAsync($"/Plant", CurrentPlant);
            createdPlant = await response.Content.ReadFromJsonAsync<Plant>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (createdPlant != null)
        {
            Snackbar.Add(I18n["PlantSuccessfullyAdded"], Severity.Success);
            MudDialog.Close(DialogResult.Ok<Plant>(createdPlant));
        }
        else
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }
    }
}