using System.Net.Http.Json;
using System.Xml.XPath;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.App.Pages.Dialogs;

public partial class EditSeedlingDialog
{
    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ISnackbar Snackbar { get; set; }

    [Parameter]
    public Seedling CurrentSeedling { get; set; } = new();

    private Plant _SelectedPlant;
    
    private DateTime? _Date
    {
        get => CurrentSeedling.Date.ToDateTime(TimeOnly.MinValue);
        set
        {
            if (value is not null)
            {
                CurrentSeedling.Date = DateOnly.FromDateTime((DateTime)value);
            }
        }
    }

    private void _Close()
    {
        MudDialog.Close();
    }

    private async Task<IEnumerable<Plant>> _SearchPlantAsync(string search)
    {
        var plants =
            await Http.GetFromJsonAsync<Paginated<IEnumerable<Plant>>>(
                $"/Plant?pageIndex=0&pageSize=10&search={search}");

        return plants.Items;
    }
    
    private async Task<IEnumerable<Variety>> _SearchVarietyAsync(string search)
    {
        var searchUrl = $"/Variety?pageIndex=0&pageSize=10&search={search}";

        if (_SelectedPlant != null)
            searchUrl = $"{searchUrl}&plantId={_SelectedPlant.Id}";
        
        var varieties =
            await Http.GetFromJsonAsync<Paginated<IEnumerable<Variety>>>(searchUrl);

        return varieties.Items;
    }
    
    private async Task _SubmitAsync()
    {
        if (CurrentSeedling.Id > 0)
            await _UpdateAsync();
        else
            await _AddAsync();
    }

    private async Task _UpdateAsync()
    {
        int updatedRowCount = 0;

        try
        {
            var response = await Http.PutAsJsonAsync<Seedling>($"/Seedling/{CurrentSeedling.Id}", CurrentSeedling);
            updatedRowCount = await response.Content.ReadFromJsonAsync<int>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (updatedRowCount == 0)
            Snackbar.Add(I18n["NothingUpdated"], Severity.Info);
        else if (updatedRowCount == 1)
            Snackbar.Add(I18n["SeedlingSuccessfullyUpdated"], Severity.Success);

        MudDialog.Close(DialogResult.Ok<int>(updatedRowCount));
    }

    private async Task _AddAsync()
    {
        Seedling createdObject = null;

        try
        {
            var response = await Http.PostAsJsonAsync($"/Seedling", CurrentSeedling);
            createdObject = await response.Content.ReadFromJsonAsync<Seedling>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (createdObject != null)
        {
            Snackbar.Add(I18n["SeedlingSuccessfullyAdded"], Severity.Success);
            MudDialog.Close(DialogResult.Ok<Seedling>(createdObject));
        }
        else
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }
    }

    protected override Task OnInitializedAsync()
    {
        if (CurrentSeedling.Variety != null)
            _SelectedPlant = CurrentSeedling.Variety.Plant;
        
        return base.OnInitializedAsync();
    }

    private void VarietyValueChanged(Variety variety)
    {
        _SelectedPlant = variety.Plant;
        CurrentSeedling.Variety = variety;
        CurrentSeedling.VarietyId = variety.Id;
    }

    private void PlantValueChanged(Plant plant)
    {
        _SelectedPlant = plant;
        CurrentSeedling.Variety = null;
        CurrentSeedling.VarietyId = 0;
    }
}