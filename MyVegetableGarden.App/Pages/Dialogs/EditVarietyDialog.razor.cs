using System.Collections;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using MudBlazor;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.App.Pages.Dialogs;

public partial class EditVarietyDialog
{
    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ISnackbar Snackbar { get; set; }

    [Parameter]
    public Variety CurrentVariety { get; set; } = new ();

    private void _Close()
    {
        MudDialog.Close();
    }

    private async Task<IEnumerable<Plant>> _SearchPlantAsync(string search)
    {
        var plants = await Http.GetFromJsonAsync<Paginated<IEnumerable<Plant>>>($"/Plant?pageIndex=0&pageSize=10&search={search}");

        return plants.Items;
    }
    
    private async Task<IEnumerable<Provider>> _SearchProviderAsync(string search)
    {
        var providers = await Http.GetFromJsonAsync<Paginated<IEnumerable<Provider>>>($"/Provider?pageIndex=0&pageSize=10&search={search}");

        return providers.Items;
    }
    
    private async Task _SubmitAsync()
    {
        if (CurrentVariety.Id > 0)
            await _UpdateAsync();
        else
            await _AddAsync();
    }
    
    private async Task _UpdateAsync()
    {
        int updatedRowCount = 0;

        try
        {
            var response = await Http.PutAsJsonAsync<Variety>($"/Variety/{CurrentVariety.Id}", CurrentVariety);
            updatedRowCount = await response.Content.ReadFromJsonAsync<int>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (updatedRowCount == 0)
            Snackbar.Add(I18n["NothingUpdated"], Severity.Info);
        else if (updatedRowCount == 1)
            Snackbar.Add(I18n["VarietySuccessfullyUpdated"], Severity.Success);

        MudDialog.Close(DialogResult.Ok<int>(updatedRowCount));
    }

    private async Task _AddAsync()
    {
        Plant createdVariety= null;

        try
        {
            var response = await Http.PostAsJsonAsync($"/Variety", CurrentVariety);
            createdVariety = await response.Content.ReadFromJsonAsync<Plant>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (createdVariety != null)
        {
            Snackbar.Add(I18n["VarietySuccessfullyAdded"], Severity.Success);
            MudDialog.Close(DialogResult.Ok<Plant>(createdVariety));
        }
        else
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }
    }

    private void ProviderValueChanged(Provider provider)
    {
        CurrentVariety.Provider = provider;
        CurrentVariety.ProviderId = provider.Id;
    }
    
    private void PlantValueChanged(Plant plant)
    {
        CurrentVariety.Plant = plant;
        CurrentVariety.PlantId = plant.Id;
    }
}