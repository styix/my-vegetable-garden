using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.App.Pages.Dialogs;

public partial class RemovePlantDialog
{
    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ISnackbar Snackbar { get; set; }

    [Parameter]
    public Plant CurrentPlant { get; set; }

    private void _Close()
    {
        MudDialog.Close();
    }

    private async Task _RemoveAsync()
    {
        try
        {
            await Http.DeleteAsync($"/Plant/{CurrentPlant.Id}");
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        Snackbar.Add(I18n["PlantSuccessfullyRemoved"], Severity.Success);

        MudDialog.Close(DialogResult.Ok(true));
    }
}