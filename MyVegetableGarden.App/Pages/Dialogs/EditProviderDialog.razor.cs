using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.App.Pages.Dialogs;

public partial class EditProviderDialog
{
    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public ISnackbar Snackbar { get; set; }

    [Parameter]
    public Provider CurrentProvider { get; set; } = new ();

    private void _Close()
    {
        MudDialog.Close();
    }

    private async Task _SubmitAsync()
    {
        if (CurrentProvider.Id > 0)
            await _UpdateAsync();
        else
            await _AddAsync();
    }
    
    private async Task _UpdateAsync()
    {
        int updatedRowCount = 0;

        try
        {
            var response = await Http.PutAsJsonAsync<Provider>($"/Provider/{CurrentProvider.Id}", CurrentProvider);
            updatedRowCount = await response.Content.ReadFromJsonAsync<int>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (updatedRowCount == 0)
            Snackbar.Add(I18n["NothingUpdated"], Severity.Info);
        else if (updatedRowCount == 1)
            Snackbar.Add(I18n["ProviderSuccessfullyUpdated"], Severity.Success);

        MudDialog.Close(DialogResult.Ok<int>(updatedRowCount));
    }

    private async Task _AddAsync()
    {
        Provider createdProvider = null;

        try
        {
            var response = await Http.PostAsJsonAsync($"/Provider", CurrentProvider);
            createdProvider = await response.Content.ReadFromJsonAsync<Provider>();
        }
        catch
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }

        if (createdProvider != null)
        {
            Snackbar.Add(I18n["ProviderSuccessfullyAdded"], Severity.Success);
            MudDialog.Close(DialogResult.Ok<Provider>(createdProvider));
        }
        else
        {
            Snackbar.Add(I18n["ErrorOccurred"], Severity.Error);
        }
    }
}