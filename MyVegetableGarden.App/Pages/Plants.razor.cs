using MyVegetableGarden.Models;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.App.Pages.Dialogs;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.App.Pages;

public partial class Plants
{
    private Paginated<List<Plant>> _PaginatedPlants = null;
    [Parameter] public Plant _SelectedPlantItem { get; set; }
    private string _SearchText;
    private MudDataGrid<Plant> _DataGrid;
    [CascadingParameter] MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }
    
    [Inject]
    public IDialogService DialogService { get; set; }

    private async Task<GridData<Plant>> _LoadGridData(GridState<Plant> state)
    {
        _PaginatedPlants = await Http.GetFromJsonAsync<Paginated<List<Plant>>>($"/Plant?pageIndex={state.Page}&pageSize={state.PageSize}&search={_SearchText}");

        return new()
        {
            Items = _PaginatedPlants.Items,
            TotalItems = _PaginatedPlants.TotalCount
        };
    }
    
    private void _OnFilterValueChanged(string searchText)
    {
        if (string.IsNullOrWhiteSpace(searchText))
            return;
        
        _SearchText = searchText;
        _DataGrid.ReloadServerData();
    }

    private async Task _AddPlantAsync()
    {
        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditPlantDialog>(I18n["Adding"], dialogOptions);
        var result = await dialog.Result;
        
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
    
    private async Task _EditPlantAsync(Plant plant)
    {
        var parameters = new DialogParameters<EditPlantDialog>();
        parameters.Add(e => e.CurrentPlant, plant);

        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditPlantDialog>(I18n["Edition"], parameters, dialogOptions);
        var result = await dialog.Result;
        
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
    
    private async Task _RemovePlantAsync(Plant plant)
    {
        var parameters = new DialogParameters<RemovePlantDialog>();
        parameters.Add(e => e.CurrentPlant, plant);

        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<RemovePlantDialog>(I18n["Removal"], parameters, dialogOptions);
        var result = await dialog.Result;
        
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
}