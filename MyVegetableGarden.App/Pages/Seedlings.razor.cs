using System.Net.Http.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using MyVegetableGarden.App.Pages.Dialogs;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.App.Pages;

public partial class Seedlings
{
    private Paginated<List<Seedling>> _PaginatedSeedlings = null;

    [Parameter]
    public Plant _SelectedSeedlingtem { get; set; }

    private string _SearchText;
    private MudDataGrid<Seedling> _DataGrid;

    [CascadingParameter]
    MudDialogInstance MudDialog { get; set; }

    [Inject]
    public HttpClient Http { get; set; }

    [Inject]
    public IDialogService DialogService { get; set; }

    private async Task<GridData<Seedling>> _LoadGridData(GridState<Seedling> state)
    {
        _PaginatedSeedlings =
            await Http.GetFromJsonAsync<Paginated<List<Seedling>>>(
                $"/Seedling?pageIndex={state.Page}&pageSize={state.PageSize}&search={_SearchText}");

        return new()
        {
            Items = _PaginatedSeedlings.Items,
            TotalItems = _PaginatedSeedlings.TotalCount
        };
    }

    private void _OnFilterValueChanged(string searchText)
    {
        if (string.IsNullOrWhiteSpace(searchText))
            return;

        _SearchText = searchText;
        _DataGrid.ReloadServerData();
    }

    private async Task _AddSeedlingAsync()
    {
        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditSeedlingDialog>(I18n["Adding"], dialogOptions);
        var result = await dialog.Result;
    
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
    
    private async Task _EditSeedlingAsync(Seedling seedling)
    {
        var parameters = new DialogParameters<EditSeedlingDialog>();
        parameters.Add(e => e.CurrentSeedling, seedling);
    
        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<EditSeedlingDialog>(I18n["Edition"], parameters, dialogOptions);
        var result = await dialog.Result;
    
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
    
    private async Task _RemoveSeedlingAsync(Seedling seedling)
    {
        var parameters = new DialogParameters<RemoveSeedlingDialog>();
        parameters.Add(e => e.CurrentSeedling, seedling);
    
        DialogOptions dialogOptions = new DialogOptions() { MaxWidth = MaxWidth.Medium, FullWidth = true };
        var dialog = await DialogService.ShowAsync<RemoveSeedlingDialog>(I18n["Removal"], parameters, dialogOptions);
        var result = await dialog.Result;
    
        if (!result.Canceled)
            _DataGrid.ReloadServerData();
    }
}