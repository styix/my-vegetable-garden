using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyVegetableGarden.Models;

public class Variety
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required]
    [MaxLength(255)]
    public string Name { get; set; }

    [Required]
    public int PlantId { get; set; }
    public Plant? Plant { get; set; }
    
    public int ProviderId { get; set; }
    public Provider? Provider { get; set; }
}