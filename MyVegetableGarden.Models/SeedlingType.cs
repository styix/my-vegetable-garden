using System.ComponentModel;

namespace MyVegetableGarden.Models;

public enum SeedlingType
{
    Inside,
    Outside
}