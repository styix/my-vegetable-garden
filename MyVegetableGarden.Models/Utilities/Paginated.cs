namespace MyVegetableGarden.Models.Utilities;

public class Paginated<T>
{
    public T Items { get; set; }

    public int TotalCount { get; set; }
}