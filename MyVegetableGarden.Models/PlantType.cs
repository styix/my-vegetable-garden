namespace MyVegetableGarden.Models;

public enum PlantType
{
    Vegetable,
    Fruit
}