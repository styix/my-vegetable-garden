using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MyVegetableGarden.Models.I18n;

namespace MyVegetableGarden.Models;

public class Plant
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required(ErrorMessageResourceName = "RequiredField", ErrorMessageResourceType = typeof(Words))]
    [MaxLength(255)]
    public string Name { get; set; }

    [Required]
    public PlantType Type { get; set; }
}