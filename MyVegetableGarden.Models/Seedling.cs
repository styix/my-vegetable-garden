using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace MyVegetableGarden.Models;

public class Seedling
{
    public Seedling()
    {
        this.Date = DateOnly.FromDateTime(DateTime.Now);;
    }
    
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required]
    public int VarietyId { get; set; }
    public Variety Variety { get; set; }

    [Required]
    public DateOnly Date { get; set; }

    public DateTime DateTime
    {
        get
        {
            return DateTime.Now;
        }
    }

    [Required]
    public SeedlingType Type { get; set; }

    [Required]
    public int Quantity { get; set; }
}