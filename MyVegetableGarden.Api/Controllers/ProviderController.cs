using Microsoft.AspNetCore.Mvc;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class ProviderController : ControllerBase
{
    private readonly IProviderService _providerService;
    
    public ProviderController(IProviderService providerService)
    {
        _providerService = providerService;
    }
    
    [HttpGet]
    public async Task<Paginated<List<Provider>>> GetAsync([FromQuery] int pageIndex = 0, [FromQuery] int pageSize = 10, [FromQuery] string search = null)
    {
        if (pageIndex < 0)
            pageIndex = 0;

        if (pageSize < 0)
            pageSize = 10;

        return await _providerService.GetAsync(pageIndex, pageSize, search);
    }

    [Route("{id}")]
    [HttpGet]
    public async Task<Provider> GetByIdAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _providerService.GetByIdAsync(id);
    }
    
    [HttpPost]
    public async Task<Provider> AddAsync([FromBody]Provider provider)
    {
        ArgumentNullException.ThrowIfNull(provider);

        return await _providerService.AddAsync(provider);
    }
    
    [Route("{id}")]
    [HttpPut]
    public async Task<int> UpdateAsync(int id, [FromBody]Provider provider)
    {
        ArgumentNullException.ThrowIfNull(provider);

        if (provider.Id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _providerService.UpdateAsync(provider);
    }

    [Route("{id}")]
    [HttpDelete]
    public async Task RemoveAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        await _providerService.RemoveAsync(id);
    }
}