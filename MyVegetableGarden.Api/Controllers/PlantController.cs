using System.Diagnostics.Contracts;
using Microsoft.AspNetCore.Mvc;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class PlantController : ControllerBase
{
    private readonly IPlantService _plantService;
    
    public PlantController(IPlantService plantService)
    {
        _plantService = plantService;
    }
    
    [HttpGet]
    public async Task<Paginated<List<Plant>>> GetAsync([FromQuery] int pageIndex = 0, [FromQuery] int pageSize = 10, [FromQuery] string search = null)
    {
        if (pageIndex < 0)
            pageIndex = 0;

        if (pageSize < 0)
            pageSize = 10;

        return await _plantService.GetAsync(pageIndex, pageSize, search);
    }

    [Route("{id}")]
    [HttpGet]
    public async Task<Plant> GetByIdAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _plantService.GetByIdAsync(id);
    }
    
    [HttpPost]
    public async Task<Plant> AddAsync([FromBody]Plant plant)
    {
        ArgumentNullException.ThrowIfNull(plant);

        return await _plantService.AddAsync(plant);
    }
    
    [Route("{id}")]
    [HttpPut]
    public async Task<int> UpdateAsync(int id, [FromBody]Plant plant)
    {
        ArgumentNullException.ThrowIfNull(plant);

        if (plant.Id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _plantService.UpdateAsync(plant);
    }

    [Route("{id}")]
    [HttpDelete]
    public async Task RemoveAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        await _plantService.RemoveAsync(id);
    }
}