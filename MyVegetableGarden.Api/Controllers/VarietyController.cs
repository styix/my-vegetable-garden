using Microsoft.AspNetCore.Mvc;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class VarietyController : ControllerBase
{
    private readonly IVarietyService _varietyService;
    
    public VarietyController(IVarietyService varietyService)
    {
        _varietyService = varietyService;
    }
    
    [HttpGet]
    public async Task<Paginated<List<Variety>>> GetAsync([FromQuery] int pageIndex = 0, [FromQuery] int pageSize = 10, [FromQuery] string search = null, [FromQuery] int plantId = 0)
    {
        if (pageIndex < 0)
            pageIndex = 0;

        if (pageSize < 0)
            pageSize = 10;

        return await _varietyService.GetAsync(pageIndex, pageSize, search, plantId);
    }

    [Route("{id}")]
    [HttpGet]
    public async Task<Variety> GetByIdAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _varietyService.GetByIdAsync(id);
    }
    
    [HttpPost]
    public async Task<Variety> AddAsync([FromBody]Variety variety)
    {
        ArgumentNullException.ThrowIfNull(variety);

        return await _varietyService.AddAsync(variety);
    }
    
    [Route("{id}")]
    [HttpPut]
    public async Task<int> UpdateAsync(int id, [FromBody]Variety variety)
    {
        ArgumentNullException.ThrowIfNull(variety);

        if (variety.Id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _varietyService.UpdateAsync(variety);
    }

    [Route("{id}")]
    [HttpDelete]
    public async Task RemoveAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        await _varietyService.RemoveAsync(id);
    }
}