using Microsoft.AspNetCore.Mvc;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class SeedlingController : ControllerBase
{
    private readonly ISeedlingService _seedlingService;
    
    public SeedlingController(ISeedlingService seedlingService)
    {
        _seedlingService = seedlingService;
    }
    
    [HttpGet]
    public async Task<Paginated<List<Seedling>>> GetAsync([FromQuery] int pageIndex = 0, [FromQuery] int pageSize = 10, [FromQuery] string search = null)
    {
        if (pageIndex < 0)
            pageIndex = 0;

        if (pageSize < 0)
            pageSize = 10;

        return await _seedlingService.GetAsync(pageIndex, pageSize, search);
    }

    [Route("{id}")]
    [HttpGet]
    public async Task<Seedling> GetByIdAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _seedlingService.GetByIdAsync(id);
    }
    
    [HttpPost]
    public async Task<Seedling> AddAsync([FromBody]Seedling seedling)
    {
        ArgumentNullException.ThrowIfNull(seedling);

        return await _seedlingService.AddAsync(seedling);
    }
    
    [Route("{id}")]
    [HttpPut]
    public async Task<int> UpdateAsync(int id, [FromBody]Seedling seedling)
    {
        ArgumentNullException.ThrowIfNull(seedling);

        if (seedling.Id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        return await _seedlingService.UpdateAsync(seedling);
    }

    [Route("{id}")]
    [HttpDelete]
    public async Task RemoveAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentException("id can't be 0 or negative");

        await _seedlingService.RemoveAsync(id);
    }
}