using Microsoft.EntityFrameworkCore;
using MyVegetableGarden.Api.Data;
using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories;

public class ProviderRepository : IProviderRepository
{
    private readonly MyVegetableGardenContext _dbContext;

    public ProviderRepository(MyVegetableGardenContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Provider>> GetAsync(int pageIndex, int pageSize, string search)
    {
        var items = _dbContext.Providers.AsQueryable();

        if (!string.IsNullOrWhiteSpace(search))
            items = items.Where(e => e.Name.ToLower().Contains(search.ToLower()));

        return await items.OrderByDescending(e => e.Id)
            .Skip(pageIndex * pageSize)
            .Take(pageSize)
            .ToListAsync();
    }

    public async Task<int> GetTotalCountAsync(string search)
    {
        if (string.IsNullOrWhiteSpace(search))
            return await _dbContext.Providers.CountAsync();

        return await _dbContext.Providers.CountAsync(e => e.Name.ToLower().Contains(search.ToLower()));
    }

    public async Task<Provider> GetByIdAsync(int id)
    {
        return await _dbContext.Providers.SingleAsync(e => e.Id == id);
    }

    public async Task<Provider> AddAsync(Provider provider)
    {
        var createdObject = _dbContext.Providers.Add(provider);
        await _dbContext.SaveChangesAsync();

        return createdObject.Entity;
    }

    public async Task<int> UpdateAsync(Provider provider)
    {
        var originalObject = _dbContext.Providers.Single(e => e.Id == provider.Id);
        _dbContext.Entry(originalObject).CurrentValues.SetValues(provider);
        int updateRows = await _dbContext.SaveChangesAsync();

        return updateRows;
    }

    public async Task RemoveAsync(int id)
    {
        var objectToRemove = _dbContext.Providers.Single(e => e.Id == id);
        _dbContext.Providers.Remove(objectToRemove);
        await _dbContext.SaveChangesAsync();
    }
}