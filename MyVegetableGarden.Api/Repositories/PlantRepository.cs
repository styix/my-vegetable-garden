using Microsoft.EntityFrameworkCore;
using MyVegetableGarden.Api.Data;
using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories;

public class PlantRepository : IPlantRepository
{
    private readonly MyVegetableGardenContext _dbContext;

    public PlantRepository(MyVegetableGardenContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Plant>> GetAsync(int pageIndex, int pageSize, string search)
    {
        var items = _dbContext.Plants.AsQueryable();

        if (!string.IsNullOrWhiteSpace(search))
            items = items.Where(e => e.Name.ToLower().Contains(search.ToLower()));

        return await items.OrderByDescending(e => e.Id)
            .Skip(pageIndex * pageSize)
            .Take(pageSize)
            .ToListAsync();
    }

    public async Task<int> GetTotalCountAsync(string search)
    {
        if (string.IsNullOrWhiteSpace(search))
            return await _dbContext.Plants.CountAsync();

        return await _dbContext.Plants.CountAsync(e => e.Name.ToLower().Contains(search.ToLower()));
    }

    public async Task<Plant> GetByIdAsync(int id)
    {
        return await _dbContext.Plants.SingleAsync(e => e.Id == id);
    }

    public async Task<Plant> AddAsync(Plant plant)
    {
        var createdObject = _dbContext.Plants.Add(plant);
        await _dbContext.SaveChangesAsync();

        return createdObject.Entity;
    }

    public async Task<int> UpdateAsync(Plant plant)
    {
        var originalObject = _dbContext.Plants.Single(e => e.Id == plant.Id);
        _dbContext.Entry(originalObject).CurrentValues.SetValues(plant);
        int updateRows = await _dbContext.SaveChangesAsync();

        return updateRows;
    }

    public async Task RemoveAsync(int id)
    {
        var objectToRemove = _dbContext.Plants.Single(e => e.Id == id);
        _dbContext.Plants.Remove(objectToRemove);
        await _dbContext.SaveChangesAsync();
    }
}