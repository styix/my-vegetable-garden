using Microsoft.EntityFrameworkCore;
using MyVegetableGarden.Api.Data;
using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories;

public class VarietyRepository : IVarietyRepository
{
    private readonly MyVegetableGardenContext _dbContext;

    public VarietyRepository(MyVegetableGardenContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Variety>> GetAsync(int pageIndex, int pageSize, string search, int plantId)
    {
        var items = _dbContext.Varieties
            .Include(e => e.Plant)
            .Include(e => e.Provider).AsQueryable();

        if (plantId > 0)
            items = items.Where(e => e.Plant.Id == plantId);
            
        if (!string.IsNullOrWhiteSpace(search))
            items = items.Where(e => e.Name.ToLower().Contains(search.ToLower()));
        
        return await items.OrderByDescending(e => e.Id)
            .Skip(pageIndex * pageSize)
            .Take(pageSize)
            .ToListAsync();
    }
    
    public async Task<int> GetTotalCountAsync(string search)
    {
        if (string.IsNullOrWhiteSpace(search))
            return await _dbContext.Varieties.CountAsync();

        return await _dbContext.Varieties.CountAsync(e => e.Name.ToLower().Contains(search.ToLower()));
    }

    public async Task<Variety> GetByIdAsync(int id)
    {
        return await _dbContext.Varieties.SingleAsync(e => e.Id == id);
    }

    public async Task<Variety> AddAsync(Variety variety)
    {
        variety.Provider = null;
        variety.Plant = null;
        
        var createdObject = _dbContext.Varieties.Add(variety);
        await _dbContext.SaveChangesAsync();

        return createdObject.Entity;
    }

    public async Task<int> UpdateAsync(Variety variety)
    {
        variety.Provider = null;
        variety.Plant = null;
        
        var originalObject = _dbContext.Varieties.Single(e => e.Id == variety.Id);
        _dbContext.Entry(originalObject).CurrentValues.SetValues(variety);
        int updateRows = await _dbContext.SaveChangesAsync();

        return updateRows;
    }

    public async Task RemoveAsync(int id)
    {
        var objectToRemove = _dbContext.Varieties.Single(e => e.Id == id);
        _dbContext.Varieties.Remove(objectToRemove);
        await _dbContext.SaveChangesAsync();
    }
}