using Microsoft.EntityFrameworkCore;
using MyVegetableGarden.Api.Data;
using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories;

public class SeedlingRepository : ISeedlingRepository
{
    private readonly MyVegetableGardenContext _dbContext;

    public SeedlingRepository(MyVegetableGardenContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Seedling>> GetAsync(int pageIndex, int pageSize, string search)
    {
        var items = _dbContext.Seedlings
            .Include(e => e.Variety)
            .Include(e => e.Variety.Plant).AsQueryable();

        if (!string.IsNullOrWhiteSpace(search))
            items = items.Where(e => e.Variety.Name.ToLower().Contains(search.ToLower())
                                     || e.Variety.Plant.Name.ToLower().Contains(search.ToLower()));
        
        return await items.OrderByDescending(e => e.Id).Skip(pageIndex * pageSize)
            .Take(pageSize)
            .ToListAsync();
    }

    public async Task<int> GetTotalCountAsync(string search)
    {
        if (string.IsNullOrWhiteSpace(search))
            return await _dbContext.Seedlings.CountAsync();

        return await _dbContext.Seedlings
            .CountAsync(e => e.Variety.Name.ToLower().Contains(search.ToLower())
                             || e.Variety.Plant.Name.ToLower().Contains(search.ToLower()));
    }

    public async Task<Seedling> GetByIdAsync(int id)
    {
        return await _dbContext.Seedlings.SingleAsync(e => e.Id == id);
    }

    public async Task<Seedling> AddAsync(Seedling seedling)
    {
        seedling.Variety = null;

        var createdObject = _dbContext.Seedlings.Add(seedling);
        await _dbContext.SaveChangesAsync();

        return createdObject.Entity;
    }

    public async Task<int> UpdateAsync(Seedling seedling)
    {
        seedling.Variety = null;

        var originalObject = _dbContext.Seedlings.Single(e => e.Id == seedling.Id);
        _dbContext.Entry(originalObject).CurrentValues.SetValues(seedling);
        int updateRows = await _dbContext.SaveChangesAsync();

        return updateRows;
    }

    public async Task RemoveAsync(int id)
    {
        var objectToRemove = _dbContext.Seedlings.Single(e => e.Id == id);
        _dbContext.Seedlings.Remove(objectToRemove);
        await _dbContext.SaveChangesAsync();
    }
}