using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories.Contracts;

public interface IPlantRepository
{
    Task<List<Plant>> GetAsync(int pageIndex, int pageSize, string search);

    Task<int> GetTotalCountAsync(string search);

    Task<Plant> GetByIdAsync(int id);

    Task<Plant> AddAsync(Plant plant);

    Task<int> UpdateAsync(Plant plant);

    Task RemoveAsync(int id);
}