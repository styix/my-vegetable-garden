using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories.Contracts;

public interface IProviderRepository
{
    Task<List<Provider>> GetAsync(int pageIndex, int pageSize, string search);

    Task<int> GetTotalCountAsync(string search);

    Task<Provider> GetByIdAsync(int id);

    Task<Provider> AddAsync(Provider provider);

    Task<int> UpdateAsync(Provider provider);

    Task RemoveAsync(int id);
}