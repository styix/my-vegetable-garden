using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories.Contracts;

public interface ISeedlingRepository
{
    Task<List<Seedling>> GetAsync(int pageIndex, int pageSize, string search);

    Task<int> GetTotalCountAsync(string search);

    Task<Seedling> GetByIdAsync(int id);

    Task<Seedling> AddAsync(Seedling seedling);

    Task<int> UpdateAsync(Seedling seedling);

    Task RemoveAsync(int id);
}