using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Repositories.Contracts;

public interface IVarietyRepository
{
    Task<List<Variety>> GetAsync(int pageIndex, int pageSize, string search, int plantId);

    Task<int> GetTotalCountAsync(string search);

    Task<Variety> GetByIdAsync(int id);

    Task<Variety> AddAsync(Variety variety);

    Task<int> UpdateAsync(Variety variety);

    Task RemoveAsync(int id);
}