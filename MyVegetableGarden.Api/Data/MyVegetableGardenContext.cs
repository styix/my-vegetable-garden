using Microsoft.EntityFrameworkCore;
using MyVegetableGarden.Models;

namespace MyVegetableGarden.Api.Data;

public class MyVegetableGardenContext : DbContext
{
    public DbSet<Plant> Plants { get; set; }
    public DbSet<Variety> Varieties { get; set; }
    public DbSet<Provider> Providers { get; set; }
    public DbSet<Seedling> Seedlings { get; set; }
    
    public MyVegetableGardenContext(DbContextOptions options) : base(options)
    {
    }
}