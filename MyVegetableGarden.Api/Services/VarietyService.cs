using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services;

public class VarietyService : IVarietyService
{
    private readonly IVarietyRepository _varietyRepository;

    public VarietyService(IVarietyRepository varietyRepository)
    {
        _varietyRepository = varietyRepository;
    }
    
    public async Task<Paginated<List<Variety>>> GetAsync(int pageIndex, int pageSize, string search, int plantId)
    {
        List<Variety> elements = await _varietyRepository.GetAsync(pageIndex, pageSize, search, plantId);
        int totalCount = await _varietyRepository.GetTotalCountAsync(search);
        
        return new Paginated<List<Variety>>()
        {
            Items = elements,
            TotalCount = totalCount
        };
    }

    public async Task<Variety> GetByIdAsync(int id)
    {
        return await _varietyRepository.GetByIdAsync(id);
    }

    public async Task<Variety> AddAsync(Variety variety)
    {
        return await _varietyRepository.AddAsync(variety);
    }

    public async Task<int> UpdateAsync(Variety variety)
    {
        return await _varietyRepository.UpdateAsync(variety);
    }

    public async Task RemoveAsync(int id)
    {
        await _varietyRepository.RemoveAsync(id);
    }
}