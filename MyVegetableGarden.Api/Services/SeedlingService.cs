using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services;

public class SeedlingService : ISeedlingService
{
    private readonly ISeedlingRepository _seedlingRepository;

    public SeedlingService(ISeedlingRepository seedlingRepository)
    {
        _seedlingRepository = seedlingRepository;
    }
    
    public async Task<Paginated<List<Seedling>>> GetAsync(int pageIndex, int pageSize, string search)
    {
        List<Seedling> elements = await _seedlingRepository.GetAsync(pageIndex, pageSize, search);
        int totalCount = await _seedlingRepository.GetTotalCountAsync(search);
        
        return new Paginated<List<Seedling>>()
        {
            Items = elements,
            TotalCount = totalCount
        };
    }

    public async Task<Seedling> GetByIdAsync(int id)
    {
        return await _seedlingRepository.GetByIdAsync(id);
    }

    public async Task<Seedling> AddAsync(Seedling seedling)
    {
        return await _seedlingRepository.AddAsync(seedling);
    }

    public async Task<int> UpdateAsync(Seedling seedling)
    {
        return await _seedlingRepository.UpdateAsync(seedling);
    }

    public async Task RemoveAsync(int id)
    {
        await _seedlingRepository.RemoveAsync(id);
    }
}