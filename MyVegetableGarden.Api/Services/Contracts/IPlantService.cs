using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services.Contracts;

public interface IPlantService
{
    Task<Paginated<List<Plant>>> GetAsync(int pageIndex, int pageSize, string search);

    Task<Plant> GetByIdAsync(int id);

    Task<Plant> AddAsync(Plant plant);

    Task<int> UpdateAsync(Plant plant);
    
    Task RemoveAsync(int id);
}