using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services.Contracts;

public interface IProviderService
{
    Task<Paginated<List<Provider>>> GetAsync(int pageIndex, int pageSize, string search);

    Task<Provider> GetByIdAsync(int id);

    Task<Provider> AddAsync(Provider provider);

    Task<int> UpdateAsync(Provider provider);
    
    Task RemoveAsync(int id);
}