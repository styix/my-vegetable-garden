using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services.Contracts;

public interface ISeedlingService
{
    Task<Paginated<List<Seedling>>> GetAsync(int pageIndex, int pageSize, string search);

    Task<Seedling> GetByIdAsync(int id);

    Task<Seedling> AddAsync(Seedling seedling);

    Task<int> UpdateAsync(Seedling seedling);
    
    Task RemoveAsync(int id);
}