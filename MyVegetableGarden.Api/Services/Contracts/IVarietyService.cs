using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services.Contracts;

public interface IVarietyService
{
    Task<Paginated<List<Variety>>> GetAsync(int pageIndex, int pageSize, string search, int plantId);

    Task<Variety> GetByIdAsync(int id);

    Task<Variety> AddAsync(Variety variety);

    Task<int> UpdateAsync(Variety variety);
    
    Task RemoveAsync(int id);
}