using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services;

public class ProviderService : IProviderService
{
    private readonly IProviderRepository _providerRepository;

    public ProviderService(IProviderRepository providerRepository)
    {
        _providerRepository = providerRepository;
    }
    
    public async Task<Paginated<List<Provider>>> GetAsync(int pageIndex, int pageSize, string search)
    {
        List<Provider> elements = await _providerRepository.GetAsync(pageIndex, pageSize, search);
        int totalCount = await _providerRepository.GetTotalCountAsync(search);
        
        return new Paginated<List<Provider>>()
        {
            Items = elements,
            TotalCount = totalCount
        };
    }

    public async Task<Provider> GetByIdAsync(int id)
    {
        return await _providerRepository.GetByIdAsync(id);
    }

    public async Task<Provider> AddAsync(Provider provider)
    {
        return await _providerRepository.AddAsync(provider);
    }

    public async Task<int> UpdateAsync(Provider provider)
    {
        return await _providerRepository.UpdateAsync(provider);
    }

    public async Task RemoveAsync(int id)
    {
        await _providerRepository.RemoveAsync(id);
    }
}