using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Api.Services.Contracts;
using MyVegetableGarden.Models;
using MyVegetableGarden.Models.Utilities;

namespace MyVegetableGarden.Api.Services;

public class PlantService : IPlantService
{
    private readonly IPlantRepository _plantRepository;

    public PlantService(IPlantRepository plantRepository)
    {
        _plantRepository = plantRepository;
    }
    
    public async Task<Paginated<List<Plant>>> GetAsync(int pageIndex, int pageSize, string search)
    {
        List<Plant> elements = await _plantRepository.GetAsync(pageIndex, pageSize, search);
        int totalCount = await _plantRepository.GetTotalCountAsync(search);
        
        return new Paginated<List<Plant>>()
        {
            Items = elements,
            TotalCount = totalCount
        };
    }

    public async Task<Plant> GetByIdAsync(int id)
    {
        return await _plantRepository.GetByIdAsync(id);
    }

    public async Task<Plant> AddAsync(Plant plant)
    {
        return await _plantRepository.AddAsync(plant);
    }

    public async Task<int> UpdateAsync(Plant plant)
    {
        return await _plantRepository.UpdateAsync(plant);
    }

    public async Task RemoveAsync(int id)
    {
        await _plantRepository.RemoveAsync(id);
    }
}