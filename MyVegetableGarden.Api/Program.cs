using Microsoft.EntityFrameworkCore;
using MyVegetableGarden.Api.Data;
using MyVegetableGarden.Api.Repositories;
using MyVegetableGarden.Api.Repositories.Contracts;
using MyVegetableGarden.Api.Services;
using MyVegetableGarden.Api.Services.Contracts;

var builder = WebApplication.CreateBuilder(args);

var serverVersion = new MySqlServerVersion(new Version(10, 5, 23));
ConfigurationManager configuration = builder.Configuration;

builder.Services.AddScoped<IPlantRepository, PlantRepository>();
builder.Services.AddScoped<IVarietyRepository, VarietyRepository>();
builder.Services.AddScoped<IProviderRepository, ProviderRepository>();
builder.Services.AddScoped<ISeedlingRepository, SeedlingRepository>();

builder.Services.AddScoped<IPlantService, PlantService>();
builder.Services.AddScoped<IVarietyService, VarietyService>();
builder.Services.AddScoped<IProviderService, ProviderService>();
builder.Services.AddScoped<ISeedlingService, SeedlingService>();

builder.Services.AddDbContext<MyVegetableGardenContext>(
    dbContextOptions => dbContextOptions
        .UseMySql(configuration.GetConnectionString("MyVegetableGarden"), serverVersion)
        // The following three options help with debugging, but should
        // be changed or removed for production.
        .LogTo(Console.WriteLine, LogLevel.Information)
        .EnableSensitiveDataLogging()
        .EnableDetailedErrors());

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(x => x.WithOrigins(configuration.GetSection("AllowedOrigins").Value.Split(","))
    .AllowCredentials()
    .WithHeaders(configuration.GetSection("AllowedHeaders").Value.Split(","))
    .WithMethods(configuration.GetSection("AllowedMethods").Value.Split(",")));

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();